#define MPI_BCAST 0x0c11    //0
#define MPI_SEND 0x0c12     //1
#define MPI_RECV 0x0c13     //2
#define MPI_BARRIER 0xc21    //3
#define MPI_ALLREDUCE 0xc22  //4
//#define MPI_MPI_Allgather
//#define MPI_Isend
//#define MPI_Wait


int getMpiType(int i){
    if(i == 0){
        return MPI_BCAST;
    }
    if(i == 1){
        return MPI_SEND;
    }
    if(i == 2){
        return MPI_RECV;
    }
    if(i == 3){
        return MPI_BARRIER;
    }
    if(i == 4){
        return MPI_ALLREDUCE;
    }
}
