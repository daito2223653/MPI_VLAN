#!/bin/sh

flg=true

# 必要なライブラリ check
echo "check requested packages..."
# mpi
mpirun > /dev/null 2>&1
if [ $? -eq 127 ]; then
  # コマンドをインストールする処理とか
  echo "you need to install MPI before install.sh"
  #exit -1
  flg=false
  echo
fi

# 新しく追加するときはここに
# example: kernel-devel
#if [ $? -eq 127 ]; then
  #
  # echo "you need to install kernel-devel before init.sh"
  # return 1
#fi

echo "check files..."

# 作ったファイルの存在チェックは以下の通り
if [ ! -e "./libsource/showPacket.c"  ]; then
  echo "showPacket.c is not exist. please uninstall this raibrary"
  $flg=false
fi
if [ ! -e "./libsource/MPI_vlan.h"  ]; then
  echo "/MPI_vlan.h is not exist. please uninstall this raibrary"
  $flg=false
fi

if [ ! $flg ]; then
  echo "checkok!!"
  echo "next, check your MPI_Setting."
fi
